/*
 * CvMotionExtractor.h
 *
 *  Created on: Dec 21, 2015
 *      Author: nairb1
 *
 *      This code does the motion feature extraction for the kinect tool tag
 */

#ifndef CVMOTIONEXTRACTOR_H_
#define CVMOTIONEXTRACTOR_H_

#include <opencv.hpp>
#include <opencv2/cudaoptflow.hpp>
#include <opencv2/cudabgsegm.hpp>

using namespace cv;
using namespace cv::cuda;

namespace honda_research_manufacturing {

typedef struct{
	double r,g,b;
} COLOR;

class CvMotionExtractor {

protected:
	bool m_use_gpu;
	Ptr<cuda::BroxOpticalFlow> m_optflow_extractor;
	Ptr<cuda::BackgroundSubtractorMOG2> m_background_subtractor;

	float scale;
	float alpha;
	float gamma;
	int inner_iterations;
	int outer_iterations;
	int solver_iterations;
	float timeStep;

	Mat m_frame, m_frame_prev,m_flow;


public:
	CvMotionExtractor();
	virtual ~CvMotionExtractor();

	// public methods
	void initialize(Mat& img);
	void updateImages(Mat& img);
	void computeOpticalFlow(Mat& flow, Mat& fl_mag,Mat& fl_dir);
	void getFlowField(Mat& u_img, Mat& v_img, Mat& flow_field);
	void drawFlowField(const Mat& flow, Mat& c_flow_map,int step, double x, const Scalar& color);
	COLOR getColor(double v, double vmin, double vmax);

	void computeForeground(Mat& fg_mask, Mat& bg_img);

	const Mat& getFrame() const;
	void setFrame(const Mat& frame);
	const Mat& getFramePrev() const;
	void setFramePrev(const Mat& framePrev);
	bool isUseGpu() const;
	void setUseGpu(bool useGpu);
};

} /* namespace honda_research_manufacturing */

#endif /* CVMOTIONEXTRACTOR_H_ */
