/*
 * CvMotionExtractor.cpp
 *
 *  Created on: Dec 21, 2015
 *      Author: nairb1
 */

#include "CvMotionExtractor.h"

namespace honda_research_manufacturing {

template <typename T> inline T clamp (T x, T a, T b)
{
    return ((x) > (a) ? ((x) < (b) ? (x) : (b)) : (a));
}

template <typename T> inline T mapValue(T x, T a, T b, T c, T d)
{
    x = clamp(x, a, b);
    return c + (d - c) * (x - a) / (b - a);
}

CvMotionExtractor::CvMotionExtractor() {
	// TODO Auto-generated constructor stub
	scale = 0.8;
	alpha = 0.25; // use different values of alpha : default (0.197), 0.25, 0.4, 0.55, 0.7, 0.85, 1.0
	gamma = 50.0;
	inner_iterations = 10;
	outer_iterations = 77;
	solver_iterations = 10;
	timeStep = 0.1;

	m_optflow_extractor  = cuda::BroxOpticalFlow::create(alpha,gamma,scale,inner_iterations,outer_iterations,solver_iterations);
	m_background_subtractor = cuda::createBackgroundSubtractorMOG2(100000,32,false);

	// set the parameters
	m_optflow_extractor->setPyramidScaleFactor(scale);
	m_optflow_extractor->setFlowSmoothness(alpha);
	m_optflow_extractor->setInnerIterations(inner_iterations);
	m_optflow_extractor->setOuterIterations(outer_iterations);
	m_optflow_extractor->setSolverIterations(solver_iterations);

}

const Mat& CvMotionExtractor::getFrame() const {
	return m_frame;
}

void CvMotionExtractor::setFrame(const Mat& frame) {
	m_frame = frame;
}

const Mat& CvMotionExtractor::getFramePrev() const {
	return m_frame_prev;
}

void CvMotionExtractor::setFramePrev(const Mat& framePrev) {
	m_frame_prev = framePrev;
}

bool CvMotionExtractor::isUseGpu() const {
	return m_use_gpu;
}

void CvMotionExtractor::initialize(Mat& img) {

	//Initializing the matrices and structures
	if(img.channels() == 3)
		cvtColor(img,m_frame,CV_BGR2GRAY);
	else
		m_frame = img.clone();
	m_frame_prev = Mat(m_frame.rows,m_frame.cols,CV_8UC1);
	m_flow = Mat(m_frame.rows,m_frame.cols,CV_32FC2);
}

void CvMotionExtractor::updateImages(Mat& img) {

	m_frame_prev = m_frame.clone();
	m_frame.release();
	if(img.channels() == 3)
		cvtColor(img,m_frame,CV_BGR2GRAY);
	else
		m_frame = img.clone();
}

void CvMotionExtractor::computeOpticalFlow(Mat& flow, Mat& fl_mag,
		Mat& fl_dir) {

	Mat frame0,frame1,fu,fv, flowImg;
	m_frame_prev.convertTo(frame0,CV_32FC1,1.0/ 255.0);
	m_frame.convertTo(frame1,CV_32FC1,1.0/255.0);
	cuda::GpuMat d_frame0;
	cuda::GpuMat d_frame1;
	cuda::GpuMat d_fu,d_fv;
	cuda::GpuMat d_flow;

	//uploading the previous and current frame into GPU memory
	d_frame0.upload(frame0);
	d_frame1.upload(frame1);

	m_optflow_extractor->calc(d_frame0,d_frame1,d_flow,cuda::Stream::Null());

	//downloading the flow vectors u and v and the images
	//d_fu.download(fu);
	//d_fv.download(fv);
	//d_frame0.download(frame0);
	//d_frame1.download(frame1);
	d_flow.download(flowImg);

	// Split the flow img
	Mat f[2];

	// TODO: Issue here
	cv::split(flowImg,f);
	fu = f[0].clone();
	fv = f[1].clone();

	// get flow field
	getFlowField(fu,fv,flow);


	// TODO:computing the flow magnitude and flow direction
	fl_mag = Mat(fu.rows,fu.cols,CV_32FC1);
	fl_dir = Mat(fu.rows,fu.cols,CV_32FC1);
	for(int i = 0 ; i < fu.rows; i++)
	{
		float* ptr_fl_mag = fl_mag.ptr<float>(i);
		float* ptr_fl_dir = fl_dir.ptr<float>(i);
		float* ptr_fu = fu.ptr<float>(i);
		float* ptr_fv = fv.ptr<float>(i);
		for(int j = 0; j < fu.cols; j++)
		{
			ptr_fl_mag[j] = (ptr_fu[j] * ptr_fu[j]) + (ptr_fv[j] * ptr_fv[j]);
			ptr_fl_mag[j] = cv::sqrt(ptr_fl_mag[j]);

			ptr_fl_dir[j] = atan2f(ptr_fv[j],ptr_fu[j]) * 180/M_PI; // stored in degrees
		}
	}

	//
}

void CvMotionExtractor::getFlowField(Mat& u, Mat& v, Mat& flow_field) {
    float maxDisplacement = 1.0f;

    for (int i = 0; i < u.rows; ++i)
    {
        const float* ptr_u = u.ptr<float>(i);
        const float* ptr_v = v.ptr<float>(i);

        for (int j = 0; j < u.cols; ++j)
        {
            float d = max(fabsf(ptr_u[j]), fabsf(ptr_v[j]));

            if (d > maxDisplacement)
                maxDisplacement = d;
        }
    }

    flow_field = Mat::zeros(u.size(), CV_8UC4);

    for (int i = 0; i < flow_field.rows; ++i)
    {
        const float* ptr_u = u.ptr<float>(i);
        const float* ptr_v = v.ptr<float>(i);


        Vec4b* row = flow_field.ptr<Vec4b>(i);

        for (int j = 0; j < flow_field.cols; ++j)
        {

        	//double val = std::sqrt(ptr_v[j]*ptr_v[j] + ptr_u[j]*ptr_u[j]);
        	float d = max(fabsf(ptr_u[j]), fabsf(ptr_v[j]));

        	if(d < maxDisplacement/16)
        		continue;

//        	COLOR c1 = getColor(ptr_v[j],0,1);
//        	COLOR c2 = getColor(ptr_u[j],0,1);
//        	COLOR c3 = getColor(val,0,1);

        	row[j][0] = 0;
        	row[j][1] = static_cast<unsigned char> (mapValue (-ptr_v[j], -maxDisplacement, maxDisplacement, 0.0f, 255.0f));
        	row[j][2] = static_cast<unsigned char> (mapValue ( ptr_u[j], -maxDisplacement, maxDisplacement, 0.0f, 255.0f));
        	row[j][3] = 255;

//        	row[j][0] = (unsigned char)(c2.b * 255);
//        	row[j][1] = (unsigned char)(c1.g * 255);
//        	row[j][2] = (unsigned char)(c3.r * 255);
//        	row[j][3] = 255;
        }
    }
}

COLOR CvMotionExtractor::getColor(double v, double vmin, double vmax) {

	COLOR c = {0.0, 0.0, 0.0};
	double dv;

	if(v < vmin)
		v = vmin;
	if(v > vmax)
		v = vmax;
	dv = vmax - vmin;

	if( v < (vmin + 0.25 * dv)){
		c.r = 0;
		c.g = 4 * (v - vmin)/dv;
		c.b = 1;
	}
	else if(v < (vmin + 0.5 * dv)){
		c.r = 0;
		c.b = 1 + 4 * (vmin + 0.25 * dv - v)/dv;
		c.g = 1;
	}
	else if(v < (vmin + 0.75 * dv)){
		c.r = 4 * (v - vmin - 0.5 * dv)/dv;
		c.b = 0;
		c.g = 1;
	}
	else{
		c.g = 1 + 4 * (vmin + 0.75 * dv - v)/dv;
		c.b = 0;
		c.r = 1;
	}

	return(c);
}

void CvMotionExtractor::drawFlowField(const Mat& flow, Mat& c_flow_map,
		int step, double a, const Scalar& color) {

	for(int y = 0; y < c_flow_map.rows; y += step)
		for(int x = 0; x < c_flow_map.cols; x += step)
		{
			const Point2f& fxy = flow.at<Point2f>(y, x);
			line(c_flow_map, Point(x,y), Point(cvRound(x+fxy.x), cvRound(y+fxy.y)),color);
			circle(c_flow_map, Point(x,y), 2, color, -1);
		}
}

// TODO: Update method
void CvMotionExtractor::computeForeground(Mat& fg_mask, Mat& bg_img) {

	Mat frame;
	frame = m_frame.clone();
	cuda::GpuMat d_frame,d_fgmask,d_bg_img;

	//uploading the previous and current frame into GPU memory
	d_frame.upload(frame);

	m_background_subtractor->apply(d_frame,d_fgmask);
	m_background_subtractor->getBackgroundImage(d_bg_img);

	//downloading the flow vectors u and v and the images
	//d_fu.download(fu);
	//d_fv.download(fv);
	//d_frame0.download(frame0);
	//d_frame1.download(frame1);
	d_fgmask.download(fg_mask);
	d_bg_img.download(bg_img);
	d_frame.download(frame);

}

void CvMotionExtractor::setUseGpu(bool useGpu) {
	m_use_gpu = useGpu;
}

CvMotionExtractor::~CvMotionExtractor() {
	// TODO Auto-generated destructor stub
}

} /* namespace honda_research_manufacturing */
