/*
 * main.cpp
 *
 *  Created on: Dec 21, 2015
 *      Author: nairb1
 */

#include "CvMotionExtractor.h"
#include <iostream>

using namespace std;
using namespace cv;
using namespace honda_research_manufacturing;

#define SOURCE_WIN "Optical Flow in Video"
#define SOURCE_WIN2 "Optical Flow in Video - Farneback"
#define SOURCE_WIN_GRAY "Video in Grayscale"
#define BG_WIN "Background Image"
#define FG_WIN "Foreground Video"

void print_help(){
	cout << "Usage : <executable> <input file>" << endl;
}

void print_cuda_info(){
	cout << "Number of cuda devices = " << cuda::getCudaEnabledDeviceCount() << endl;
	cuda::DeviceInfo cuda_info;
	cout << "Device name : " << cuda_info.name() << endl;
}

int main(int argc,char** argv){

	VideoCapture capture;
	CvMotionExtractor m_extract;

	if(argc < 2){
		print_help();
	}

	// cuda initialization
	print_cuda_info();

	capture.open(argv[1]); //opening the video file
	Mat frame_full;
	Mat flow_mag,flow_dir,frame_ycbcr,flow_map;
	Mat flow_field;
	int frame_count = 0;
	namedWindow(SOURCE_WIN,WINDOW_NORMAL);
	//namedWindow(SOURCE_WIN2,WINDOW_NORMAL);
	namedWindow(BG_WIN,WINDOW_NORMAL);
	namedWindow(FG_WIN,WINDOW_NORMAL);

	//namedWindow(SOURCE_WIN2,WINDOW_NORMAL);
	if(capture.isOpened())
	{
		// reading the video frames
		for(;;)
		{

			capture >> frame_full;
			if(frame_full.empty())
			{
				//cout << "End of Video File" <<endl;
				break;
			}

			frame_count++;
			Mat frame;
			cv::pyrDown(frame_full,frame);
			//cv::pyrUp(frame,frame);

			imshow(SOURCE_WIN,frame);


			//Computing optical flow
			if(frame_count == 1)
			{
				// Initializing the optical flow structure
				m_extract.initialize(frame);

			}
			else
			{
				//update the images
				m_extract.updateImages(frame);

				//COMPUING OPTICAL FLOW

				//load points of the frame
				//frame.copyTo(flow_map);

				//m_extract.computeOpticalFlow(flow_field,flow_mag,flow_dir);

				//Mat c_flow_map = frame.clone();
				//m_extract.drawFlowField(flow_field,c_flow_map,3,1,Scalar(0,255,0));

				//imshow(SOURCE_WIN2,flow_field);
				Mat fg_mask,bg_img;
				m_extract.computeForeground(fg_mask,bg_img);
				imshow(FG_WIN,fg_mask);
				imshow(BG_WIN,bg_img);
			}
			if(waitKey(80) >= 0) break;

		}

	}
	else cout << "Error Opening the video file" <<endl;


	capture.release();

}


