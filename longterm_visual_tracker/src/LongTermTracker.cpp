/*
 * LongTermTracker.cpp
 *
 *  Created on: Feb 4, 2016
 *      Author: nairb1
 *      This is a simple usage of the TLD tracker from OpenCV.
 *      Its purpose is to evaluate the performance of the tracker for tool tracking applications
 *
 *		UI Software Design
 *		1) The user should be able to read a video file, or stream live from a camera
 *		2) If its a video file, he should be able to pause the video and perform a selection of the object.
 *			2.1)
 *		3) If its a live stream mode,
 *			3.1) the user should be able to activate the tool tag positioning algorithm on and off.
 *			3.2) If tool tracking is on, the user should be able to toggle tracking on and off
 *
 *
 */

/* Main Issues in Tracking
 * 1) Motion blur introduced due to fast movement!
 * 2) Rotation of the object needs to be accounted for in the tracking.
 * 3) Random sampling causes different tracking results.
 */

#include <iostream>

#include "CVToolTracker.h"

#define SOURCE_WIN "Video"
#define SOURCE_WIN2 "Selection Frame"

using namespace std;
using namespace cv;
using namespace honda_research_manufacturing;

void print_help(){
	cout << "Usage : <executable> <input file>" << endl;
}


int main(int argc,char** argv){

	// Input a video file
	VideoCapture capture;

	// initialize a video writer
	//VideoWriter writer;

	// TODO: Initialize the tracker
	CVToolTracker* tool_tracker = new CVToolTracker();

	if(argc < 3){
		print_help();
	}

	// press a key to stop video and get a region of interest
	capture.open(argv[1]); //opening the video file
	Size fr_size;
	fr_size.width = capture.get(CV_CAP_PROP_FRAME_WIDTH);
	fr_size.height = capture.get(CV_CAP_PROP_FRAME_HEIGHT);

	//writer.open(argv[2],VideoWriter::fourcc('P','I','M','1'),30,fr_size);

	Mat frame;
	Mat frame_bck;
	Mat mask;
	namedWindow(SOURCE_WIN,WINDOW_NORMAL);
	namedWindow(SOURCE_WIN2,WINDOW_NORMAL);
	Rect2d selection;
	bool track_start = false;

	int frame_count = 0;
	int time_lapse = 30;

	if(capture.isOpened()){
		// reading the video frames
		for(;;){
			capture >> frame;
			if(frame.empty())
				break;
			frame_count++;

			// start the tracking
			if(track_start){
				mask = tool_tracker->generateMask(frame);
				imshow(SOURCE_WIN2,mask);

				// write the video frame
				//writer << frame;

			}

			imshow(SOURCE_WIN,frame);
			char c = waitKey(time_lapse);

			if(c == 27)
				break;

			if(frame_count == 47){
				cout << "Frame number " << frame_count << endl;
				frame_bck = frame.clone();
				imshow(SOURCE_WIN2,frame_bck);
				cout << "Video paused" << endl;
				cout << "Find the roi from the window and press any key" <<endl;
				char c1 = waitKey(0);

				cout << "Please input the roi selection parameters for the object to track." << endl;

				cv::Point leftPt,rightPt;
				//cin >> leftPt.x;
				//cin >> leftPt.y;
				//cin >> rightPt.x;
				//cin >> rightPt.y;
				//leftPt.x = 805; leftPt.y = 431; rightPt.x = 897; rightPt.y = 495;
				//leftPt.x = 297; leftPt.y = 300; rightPt.x = 784; rightPt.y = 451;
				//leftPt.x = 712; leftPt.y = 264; rightPt.x = 799; rightPt.y = 306;

				// frame 47
				leftPt.x = 639; leftPt.y = 446; rightPt.x = 679; rightPt.y = 551;
				//leftPt.x = 618; leftPt.y = 400; rightPt.x = 699; rightPt.y = 560;

				// frame 197
				//leftPt.x = 565; leftPt.y = 455; rightPt.x = 597; rightPt.y = 543;

				// frame 331
				//leftPt.x = 730; leftPt.y = 380; rightPt.x = 790; rightPt.y = 540;

				selection.x = double(leftPt.x); selection.y = double(leftPt.y);
				selection.width = double(std::abs(rightPt.x - leftPt.x));
				selection.height = double(std::abs(rightPt.y - leftPt.y));

				//printf("%d %d %d %d\n", selection.x, selection.y, selection.width, selection.height);
				//Mat roi(frame_bck, selection);
				rectangle(frame_bck,leftPt,rightPt,Scalar(255,0,0),5);
				imshow(SOURCE_WIN2,frame_bck);

				cout << "Press any key to continue" <<endl;
				c1 = waitKey(0);

				// initialize the tracker with a bounding region
				tool_tracker->track(frame,selection);

				//tool_tracker->drawBox(frame);
				track_start = true;
			}

		}

	}
	else cout << "Error Opening the video file" <<endl;


	delete tool_tracker;
	capture.release();
	//writer.release();




}


