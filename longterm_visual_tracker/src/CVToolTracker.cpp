/*
 * CVToolTracker.cpp
 *
 *  Created on: Feb 17, 2016
 *      Author: nairb1
 */

#include "CVToolTracker.h"

namespace honda_research_manufacturing {

CVToolTracker::CVToolTracker() {
	// TODO Auto-generated constructor stub
	// Default
	//trackerTLD = cv::TrackerTLD::createTracker();
	//trackerMedianFlow = cv::TrackerMedianFlow::createTracker();

	//tracker = trackerTLD; // morph it to a generic tracker implementation
	//tracker = cv::Tracker::create("MIL");


	// add a feature set to the tracker
	//cv::Ptr<cv::TrackerFeature> trackerFeature = new cv::TrackerFeatureHAAR();
	//tracker->featureSet->addTrackerFeature(trackerFeature);
	// set the parameters
	parameters.samplerSearchWinSize = 60;
	parameters.samplerInitInRadius = 3;
	parameters.samplerTrackInRadius = 3;
	parameters.samplerTrackMaxPosNum = 500;
	parameters.samplerTrackMaxNegNum = 300;
	//parameters.samplerTrackInRadius = 10;
	//parameters.featureSetNumFeatures = 50;
	//parameters.samplerTrackInRadius = 20;

	//std::cout << "Sampler Init Radius = " << parameters.samplerInitInRadius << std::endl;
	//std::cout << "Sampler Init Max Neg Num = " << parameters.samplerInitMaxNegNum << std::endl;
//	std::cout << "Sampler Search Win Size = " << parameters.samplerSearchWinSize << std::endl;
//	std::cout << "Sampler Track In Radius = " << parameters.samplerTrackInRadius << std::endl;
//	std::cout << "Sampler Track Max Pos Num Track = " << parameters.samplerTrackMaxPosNum << std::endl;
//	std::cout << "Sampler Track Max Neg Num Track = " << parameters.samplerTrackMaxNegNum << std::endl;
//	std::cout << "Number of Features in feature set = " << parameters.featureSetNumFeatures << std::endl;

	trackerMIL = cv::TrackerMIL::createTracker(parameters);

}

CVToolTracker::CVToolTracker(TrackerType type) {
}

void CVToolTracker::track(cv::Mat img, cv::Rect2d roi) {

	// TODO: As we increase the size of the search region (pre-defined), we sometimes get locked to the wrong tag
	// TODO: So, what we need is a verification algorithm to verify if our tracker estimate is correct.
	// TODO: One way is do region-based matching between the detected region in the current frame and the previous corrected region.
	// TODO: If the image characteristics are wrong, then the tracker matches a wrong image.
	// TODO: Indicator when drift has happened in the tracker.
	// TODO: How to check if drift happens?
	// TODO: When drift happens, the tracker needs to re-initialize
	// TODO: One way, is to obtain the median velocity of the region and track the velocity.
	// TODO: Compute the velocity using optical flow.
	// TODO: Tracker MIL tracks the changes in the region, and tracker Kalman tracks the linear velocity shifts.
	// TODO: If there is an abrupt shift in velocity, a drift has occurred.
	// TODO: So, when tracking or updating, we first need to predict and see if the detected location is

	// if no region of interest is provided, just track
	if(roi == cv::Rect2d()){
		bool flagDetect = trackerMIL->update(img,this->m_boundingBox);
		//bool flagDetect = this->trackerMedianFlow->update(img,this->m_boundingBox);
		//std::cout << flagDetect << std::endl;
	}
	else{
		// reinitialize the tracker
		this->setBoundingBox(roi);

		// initialize the tracker
		this->isInitialized = trackerMIL->init(img,this->m_boundingBox);
		//this->isInitialized = this->trackerMedianFlow->init(img, this->m_boundingBox);

		// TODO: update the tracker (not sure if we have to update the tracker?)

	}
}

const cv::Rect2d& CVToolTracker::getBoundingBox() const {
	return m_boundingBox;
}

void CVToolTracker::setBoundingBox(const cv::Rect2d& boundingBox) {
	m_boundingBox = boundingBox;
}

bool CVToolTracker::isReInitialize() const {
	return m_reInitialize;
}

void CVToolTracker::setReInitialize(bool reInitialize) {
	m_reInitialize = reInitialize;
}

TrackerType CVToolTracker::getType() const {
	return m_type;
}

void CVToolTracker::setType(TrackerType type) {
	m_type = type;
}

CVToolTracker::~CVToolTracker() {
	// TODO Auto-generated destructor stub
}

void CVToolTracker::drawBox(cv::Mat& img) {
	cv::Rect2d tracked_roi = this->getBoundingBox();
	cv::Point pt1,pt2;
	int paintSize = 5;
	pt1.x = (int)tracked_roi.x;
	pt1.y = (int)tracked_roi.y;
	pt2.x = pt1.x + (int)tracked_roi.width;
	pt2.y = pt1.y + (int)tracked_roi.height;
	//std::cout << pt1 << " ; " << pt2 <<std::endl;
	cv::rectangle(img,pt1,pt2,cv::Scalar(0,0,255),paintSize);
}

// This function call will generate a mask region from the global tracking scheme
cv::Mat CVToolTracker::generateMask(cv::Mat& img) {
	this->track(img);
	this->drawBox(img);
	cv::Rect2d tracked_roi = this->getBoundingBox();
	cv::Point center_pt;
	int radius;

	radius = (tracked_roi.width > tracked_roi.height)? (int)(tracked_roi.width/2) : (int)(tracked_roi.height/2);
	center_pt.x = (int)tracked_roi.x + (int)(tracked_roi.width/2);
	center_pt.y = (int)tracked_roi.y + (int)(tracked_roi.height/2);

	// TODO: Generate circular mask by taking the length of the segment
	cv::Mat mask = cv::Mat::zeros(img.rows,img.cols,CV_8UC1);
	cv::circle(mask,center_pt,radius, cv::Scalar(255,255,255),-1);

	return mask;
}

} /* namespace honda_research_manufacturing */
