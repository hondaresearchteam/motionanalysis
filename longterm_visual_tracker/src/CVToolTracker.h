/*
 * CVToolTracker.h
 *
 *  Created on: Feb 17, 2016
 *      Author: nairb1
 *      Notes:
 *      	The 'tracker' class attribute instantiates three different components of the tracker
 *      	1) TrackerSampler : Object which computes the patches over the frame based on the last target location.
 *      	2) TrackerFeatureSet: Class that manages the Features such as HAAR, HOG, LBP etc.
 *      	3) TrackerModel: Internal representation of the target, also known as 'appearance model'
 *      	Can be accessed by tracker->featureSet; etc..
 */

#ifndef CVTOOLTRACKER_H_
#define CVTOOLTRACKER_H_
#include <string>
#include <opencv.hpp>
#include <opencv2/tracking/tracker.hpp>
#include <highgui.hpp>
#include <iostream>

namespace honda_research_manufacturing {

// TODO: Not used for now
typedef enum _TrackerType{
	Boosting,
	MIL,
	MedFlow,
	TLD
} TrackerType;

class CVToolTracker {

protected:
	// string to denote what tracking mechanism to use
	TrackerType m_type;
	bool m_reInitialize;
	bool isInitialized;
	cv::Rect2d m_boundingBox; // bounding box of object to be initialized

	// TODO: Need to put it under one name using polymorphism

	// using polymorphism, generate different versions
	//cv::Ptr<cv::Tracker> tracker; // generic tracker
	//cv::Ptr<cv::TrackerTLD> trackerTLD;
	//cv::Ptr<cv::TrackerMedianFlow> trackerMedianFlow;

	// Declaring the tracker and its parameters
	cv::Ptr<cv::TrackerMIL> trackerMIL;
	cv::TrackerMIL::Params parameters;

public:
	CVToolTracker();
	CVToolTracker(TrackerType type);
	virtual ~CVToolTracker();
	cv::Mat generateMask(cv::Mat& img);

	// getters and setters
	const cv::Rect2d& getBoundingBox() const;
	void setBoundingBox(const cv::Rect2d& boundingBox);
	bool isReInitialize() const;
	void setReInitialize(bool reInitialize);
	TrackerType getType() const;
	void setType(TrackerType type);

	// function to call the track/initialize function
	void track(cv::Mat img, cv::Rect2d roi = cv::Rect()); // track function
	void drawBox(cv::Mat& img);



};

} /* namespace honda_research_manufacturing */

#endif /* CVTOOLTRACKER_H_ */
