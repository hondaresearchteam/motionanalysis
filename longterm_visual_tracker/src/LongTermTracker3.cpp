/*
 * LongTermTracker.cpp
 *
 *  Created on: Feb 4, 2016
 *      Author: nairb1
 *      This is a simple usage of the TLD tracker from OpenCV.
 *      Its purpose is to evaluate the performance of the tracker for tool tracking applications
 *
 *		UI Software Design
 *		1) The user should be able to read a video file, or stream live from a camera
 *		2) If its a video file, he should be able to pause the video and perform a selection of the object.
 *			2.1)
 *		3) If its a live stream mode,
 *			3.1) the user should be able to activate the tool tag positioning algorithm on and off.
 *			3.2) If tool tracking is on, the user should be able to toggle tracking on and off
 *
 *
 */

#include <iostream>

#include "CVToolTracker.h"

#define SOURCE_WIN "Video"
#define SOURCE_WIN2 "Selection Frame"

using namespace std;
using namespace cv;
using namespace honda_research_manufacturing;

int paintSize = 5;

void print_help(){
	cout << "Usage : <executable> <input file>" << endl;
}


int main(int argc,char** argv){

	// Input a video file
	VideoCapture capture;

	// TODO: Initialize the tracker
	CVToolTracker* tool_tracker = new CVToolTracker();

	if(argc < 2){
		print_help();
	}

	// press a key to stop video and get a region of interest
	capture.open(argv[1]); //opening the video file
	Mat frame;
	Mat frame_bck;
	namedWindow(SOURCE_WIN,WINDOW_NORMAL);
	Rect2d selection;
	bool track_start = false;

	int frame_count = 0;

	if(capture.isOpened()){
		// reading the video frames
		for(;;){
			capture >> frame;
			if(frame.empty())
				break;
			frame_count++;

			// start the tracking
			if(track_start){
				tool_tracker->track(frame);
				Rect2d tracked_roi = tool_tracker->getBoundingBox();
				Point pt1,pt2;
				pt1.x = (int)tracked_roi.x;
				pt1.y = (int)tracked_roi.y;
				pt2.x = pt1.x + (int)tracked_roi.width;
				pt2.y = pt1.y + (int)tracked_roi.height;
				std::cout << pt1 << " ; " << pt2 <<std::endl;
				rectangle(frame,pt1,pt2,Scalar(0,0,255),paintSize);
			}

			imshow(SOURCE_WIN,frame);
			char c = waitKey(400);

			if(frame_count == 47){
				cout << "Frame number " << frame_count << endl;
				frame_bck = frame.clone();
				namedWindow(SOURCE_WIN2,WINDOW_NORMAL);
				imshow(SOURCE_WIN2,frame_bck);
				cout << "Video paused" << endl;
				cout << "Find the roi from the window and press any key" <<endl;
				char c1 = waitKey(0);

				cout << "Please input the roi selection parameters for the object to track." << endl;

				cv::Point leftPt,rightPt;
				//cin >> leftPt.x;
				//cin >> leftPt.y;
				//cin >> rightPt.x;
				//cin >> rightPt.y;
				//leftPt.x = 805; leftPt.y = 431; rightPt.x = 897; rightPt.y = 495;
				//leftPt.x = 297; leftPt.y = 300; rightPt.x = 784; rightPt.y = 451;
				//leftPt.x = 712; leftPt.y = 264; rightPt.x = 799; rightPt.y = 306;
				leftPt.x = 639; leftPt.y = 446; rightPt.x = 679; rightPt.y = 551;

				selection.x = double(leftPt.x); selection.y = double(leftPt.y);
				selection.width = double(std::abs(rightPt.x - leftPt.x));
				selection.height = double(std::abs(rightPt.y - leftPt.y));

				//printf("%d %d %d %d\n", selection.x, selection.y, selection.width, selection.height);
				//Mat roi(frame_bck, selection);
				rectangle(frame_bck,leftPt,rightPt,Scalar(255,0,0),paintSize);

				imshow(SOURCE_WIN2,frame_bck);

				cout << "Press any key to continue" <<endl;
				c1 = waitKey(0);

				// initialize the tracker with a bounding region
				tool_tracker->track(frame,selection);
				track_start = true;
			}

			// Start tracking
			// press a certain key to stop
			//c = waitKey(100);


		}

	}
	else cout << "Error Opening the video file" <<endl;


	delete tool_tracker;
	capture.release();




}


