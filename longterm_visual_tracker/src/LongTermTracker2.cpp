/*
 * LongTermTracker.cpp
 *
 *  Created on: Feb 4, 2016
 *      Author: nairb1
 *      This is a simple usage of the TLD tracker from OpenCV.
 *      Its purpose is to evaluate the performance of the tracker for tool tracking applications
 *
 *		UI Software Design
 *		1) The user should be able to read a video file, or stream live from a camera
 *		2) If its a video file, he should be able to pause the video and perform a selection of the object.
 *			2.1)
 *		3) If its a live stream mode,
 *			3.1) the user should be able to activate the tool tag positioning algorithm on and off.
 *			3.2) If tool tracking is on, the user should be able to toggle tracking on and off
 *
 *
 */

#include <iostream>

#include "CVToolTracker.h"

#define SOURCE_WIN "Video"
#define SOURCE_WIN2 "Selection Frame"

using namespace std;
using namespace cv;
using namespace honda_research_manufacturing;

bool selectObject = false;
Rect selection;
Point origin;
int trackObject = 0;
int finishPartFeature = 0;
Mat image;
Mat annotated_image;
int paintSize = 5;

void print_help(){
	cout << "Usage : <executable> <input file>" << endl;
}

static void onMouse( int event, int x, int y, int, void* )
{
    if( selectObject )
    {
        selection.x = MIN(x, origin.x);
        selection.y = MIN(y, origin.y);
        selection.width = std::abs(x - origin.x);
        selection.height = std::abs(y - origin.y);

        selection &= Rect(0, 0, image.cols, image.rows);
    }

    switch( event )
    {
    case CV_EVENT_LBUTTONDOWN:
        origin = Point(x,y);
        selection = Rect(x,y,0,0);
        selectObject = true;
        break;
    case CV_EVENT_LBUTTONUP:
        selectObject = false;
        if( selection.width > 0 && selection.height > 0 )
            trackObject = -1;
        break;

//    case CV_EVENT_LBUTTONDBLCLK:
//    	selectObject = false;
//    	if( selection.width > 0 && selection.height > 0 ){
//    		trackObject = -1;
//    		finishPartFeature = 1;
//    	}
//    	break;
    }

}


int main(int argc,char** argv){

	// Input a video file
	VideoCapture capture;

	// TODO: Initialize the tracker


	if(argc < 2){
		print_help();
	}

	// press a key to stop video and get a region of interest
	capture.open(argv[1]); //opening the video file
	Mat frame;
	Mat frame_bck;
	cv::namedWindow(SOURCE_WIN,WINDOW_NORMAL);


	if(capture.isOpened()){
		// reading the video frames
		for(;;){
			capture >> frame;
			if(frame.empty())
				break;

			imshow(SOURCE_WIN,frame);

			// press a certain key to stop
			char c = waitKey(200);

			if(c == 'p'){
				cout << "Video paused" << endl;
				cout << "Please annotate an object to track." << endl;

				frame_bck = frame.clone();
				cv::namedWindow(SOURCE_WIN2,WINDOW_NORMAL);
				cv::setMouseCallback( SOURCE_WIN2, onMouse, 0 ); // setting the mouse call back on the window
				imshow(SOURCE_WIN2,frame_bck);

				// iterative process of selecting a bounding box region untill a letter is pressed to resume
				for(;;){

					// TODO: Annotate the object
					if(selectObject && selection.width > 0 && selection.height > 0)
					{
						// to show what is being selected
						Mat roi(frame_bck, selection);
						bitwise_not(roi, roi);
						printf("%d %d %d %d\n", selection.x, selection.y, selection.width, selection.height);
					}

					if(trackObject == -1){
						if(selection.width > 0 && selection.height > 0)
							rectangle(frame_bck,Point(selection.x,selection.y),Point(selection.x+selection.width,selection.y+selection.height),Scalar(255,0,0),paintSize);
					}
					char c1 = waitKey(30);

					if(c1 == 'd'){

						// Show the rectangle on the image
						rectangle(frame_bck,Point(selection.x,selection.y),Point(selection.x+selection.width,selection.y+selection.height),Scalar(0,255,255),paintSize);

						// TODO: Set the bounding box region of the tracker

						//
						break;
					}


					//
					if(c1 == 'q'){

						break;
					}
				}
			}

			// TODO: if selection is made, do tracking, if not continue



		}

	}
	else cout << "Error Opening the video file" <<endl;


	capture.release();




}


